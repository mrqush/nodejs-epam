const express = require('express');
const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');

const api = express();
const jsonParser = express.json();

const filesFolder = path.join(__dirname, 'files');

try {
    if (!fs.existsSync(filesFolder)) {
        fs.mkdirSync(filesFolder);
    }
} catch (err) {
    console.error(err);
}

async function getLocalFiles() {
    let files = [];
    const dir = await fsPromises.readdir('files');
    for (const filename of dir) {
        files.push({
            filename,
            content: await fsPromises.readFile('files/' + filename, 'utf-8')
        })
    }
    return files;
}

function checkFileExtension(filename) {
    const allowedFiles = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    const regex = new RegExp(`([a-zA-Z0-9_\\.\-:])+(${allowedFiles.join('|')})$`);
    return regex.test(filename);
}

function formattedTime() {
    let result = "";
    const date = new Date();
    result += "[" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "] ";
    return result;
}

api.get('/', async (req, res) => {
    res.send('Start page');
})

api.post('/api/files', jsonParser, async (req, res) => {
    try {
        const files = await getLocalFiles();
        const filename = req.body.filename;
        const content = req.body.content;
        if (!checkFileExtension(filename) || files.find(item => item.filename === filename)) {
            throw new Error('Invalid input');
        }
        if (!content.trim()) {
            throw new Error('Please specify "content" parameter')
        }
        fs.writeFile(path.join('files', filename), content, (err) => {
            if (err) {
                throw err;
            }
            files.push({
                filename,
                content: content
            });
        });
        res.status(200).send({
            message: 'File created successfully'
        })
        console.log(formattedTime() + 'Create file: ' + filename);
    } catch (err) {
        res.status(400).send({
            message: err.message
        });
        console.error(formattedTime() + 'Error: ' + err.message);
    }
})

api.get('/api/files', async (req, res) => {
    const files = await getLocalFiles();
    res.status(200).send({
        message: 'Success',
        files: files.map(item => item.filename)
    })
    console.log(formattedTime() + 'Get files list');
})

api.get('/api/files/:filename', async (req, res) => {
    try {
        const files = await getLocalFiles();
        const filename = req.params.filename;
        if (!checkFileExtension(filename)) {
            throw new Error('Invalid input');
        }
        const file = files.find(item => item.filename === filename);
        if (!file) {
            throw new Error(`No file with '${filename}' filename found`);
        }
        res.status(200).send({
            'message': 'Success',
            'filename': filename,
            'content': file.content,
            'extension': filename.substring(filename.lastIndexOf('.') + 1, filename.length),
            'uploadedDate': fs.statSync('files/' + filename).birthtime
        })
        console.log(formattedTime() + 'Get file: ' + filename);
    } catch (err) {
        res.status(400).send({
            message: err.message
        });
        console.error(formattedTime() + 'Error: ' + err.message);
    }
})

api.put('/api/files/:filename', jsonParser, async (req, res) => {
    try {
        const files = await getLocalFiles();
        const filename = req.params.filename;
        const content = req.body.content;
        if (!checkFileExtension(filename)) {
            throw new Error('Invalid input');
        }
        let file = files.find(item => item.filename === filename);
        if (!file) {
            throw new Error(`No file with '${filename}' filename found`);
        }
        if (!content.trim()) {
            throw new Error('Please specify "content" parameter')
        }

        fs.writeFile('files/' + filename, content, (err) => {
            if (err) throw err;
        })

        files = await getLocalFiles();
        file = files.find(item => item.filename === filename);
        res.status(200).send(file);
        console.log(formattedTime() + 'Edit file: ' + filename);
    } catch (err) {
        res.status(400).send({
            message: err.message
        });
        console.error(formattedTime() + 'Error: ' + err.message);
    }
})

api.delete('/api/files/:filename', async (req, res) => {
    try {
        const files = await getLocalFiles();
        const filename = req.params.filename;
        if (!checkFileExtension(filename)) {
            throw new Error('Invalid input');
        }
        const file = files.find(item => item.filename === filename);
        if (!file) {
            throw new Error('File not found');
        }

        fs.unlink(path.join('files', filename), (err) => {
            if (err) throw err;
        })
        res.status(200).send({
            message: 'Successful operation'
        });
        console.log(formattedTime() + 'Delete file: ' + filename);
    } catch (err) {
        res.status(400).send({
            message: err.message
        });
        console.error(formattedTime() + 'Error: ' + err.message);
    }
})

api.listen(8080, console.log('Server is running on http://localhost:8080'));